public class Group {
    private final int groupId;
    private final int groupSize;
    private final int moduleIds[];
    private final int numberOfSlots;

    public Group(int groupId, int groupSize, int numberOfSlots,int moduleIds[]){
        this.groupId = groupId;
        this.groupSize = groupSize;
        this.moduleIds = moduleIds;
        this.numberOfSlots = numberOfSlots;
    }

    public int getGroupId(){
        return this.groupId;
    }

    public int getGroupSize(){
        return this.groupSize;
    }

    public int[] getModuleIds(){
        return this.moduleIds;
    }

    public int getNumberOfSlots(){
        return this.numberOfSlots;
    }
}
