public class Module {
    private final int moduleId;
    private final String moduleCode;
    private final String module;
    private final int professorIds[];
    private final int numberOfSlots;

    public Module(int moduleId, String moduleCode, String module, int numberOfSlots, int... professorIds){
        this.moduleId = moduleId;
        this.moduleCode = moduleCode;
        this.module = module;
        this.numberOfSlots = numberOfSlots;
        this.professorIds = professorIds;
    }

    public int getModuleId(){
        return this.moduleId;
    }

    public String getModuleCode(){
        return this.moduleCode;
    }

    public String getModuleName(){
        return this.module;
    }

    public int getNumberOfSlots(){
        return this.numberOfSlots;
    }

    public int getRandomProfessorId(){
        int professorId = professorIds[(int) (professorIds.length * Math.random())];
        return professorId;
    }
}
