import java.util.ArrayList;

public class TimetableGA {

    public static void main(String[] args) {
        Timetable timetable = initializeTimetable();
        GeneticAlgorithm ga = new GeneticAlgorithm(50, 0.01, 0.8, 2, 5);
        Population population = ga.initPopulation(timetable);
        ga.evalPopulation(population, timetable);
        int generation = 1;
        
        while (ga.isTerminationConditionMet(generation, 2000) == false && ga.isTerminationConditionMet(population) == false) {
            System.out.println("G" + generation + " Best fitness: " + population.getFittest(0).getFitness());
            population = ga.crossoverPopulation(population);
            population = ga.mutatePopulation(population, timetable);
            ga.evalPopulation(population, timetable);
            generation++;
        }

        timetable.createClasses(population.getFittest(0));
        System.out.println();
        System.out.println("Solution found in " + generation + " generations");
        System.out.println("Final solution fitness: " + population.getFittest(0).getFitness());
        System.out.println("Clashes: " + timetable.calcClashes());

        System.out.println();
        Class classes[] = timetable.getClasses();
        int classIndex = 1;
        for (Class bestClass : classes) {
            System.out.println("Class " + classIndex + ":");
            System.out.println("Module: " + timetable.getModule(bestClass.getModuleId()).getModuleName());
            System.out.println("Group: " + timetable.getGroup(bestClass.getGroupId()).getGroupId());
            System.out.println("Room: " + timetable.getRoom(bestClass.getRoomId()).getRoomNumber());
            System.out.println("Professor: " + timetable.getProfessor(bestClass.getProfessorId()).getProfessorName());
            System.out.println("Time: " + timetable.getTimeslot(bestClass.getTimeslotId()).getTimeslot());
            System.out.println("-----");
            classIndex++;
        }
    }

	private static Timetable initializeTimetable() {
		Timetable timetable = new Timetable();
        ArrayList<Double> spin = new ArrayList<Double>();
        for (Double i = 8.0; i <18.0; i += 0.5) {
            spin.add(i);
        }
		timetable.addRoom(1, "11", 25);
		timetable.addRoom(2, "12", 30);
		timetable.addRoom(3, "24", 40);

        for (int d = 0; d<7; d++)
            for (int i = 0 ; i < spin.size(); i++)
		        timetable.addTimeslot((20*d)+i, "Day: " + d + ", Hour: " + spin.get(i).toString());

		timetable.addProfessor(1, "Mr A");
		timetable.addProfessor(2, "Mr B");

		timetable.addModule(1, "cs1", "Math", 4,new int[] { 1});
		timetable.addModule(2, "en1", "Stat", 2, new int[] { 1});
		timetable.addModule(3, "ma1", "Geometry", 3, new int[] {2});

		timetable.addGroup(1, 25, 4,new int[] { 1 });
		timetable.addGroup(2, 40, 2,new int[] { 2 });
		timetable.addGroup(3, 27, 2,new int[] { 2 });
		timetable.addGroup(4, 30, 3,new int[] { 3 });
		return timetable;
	}
}
